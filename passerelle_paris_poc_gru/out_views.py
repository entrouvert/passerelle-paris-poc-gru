# passerelle - uniform access to multiple data sources and services
# Copyright (C) 2015-2016  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import uuid
import json
import logging

import requests

from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic.base import View
from django.views.generic.detail import SingleObjectMixin
from django.http import HttpResponse

from .models import ParisPocGru

from passerelle import utils


class MissingKeyException(Exception):
    http_status = 400

class DemandNotFoundException(Exception):
    http_status = 404


class DemandBaseView(View, SingleObjectMixin):
    model = ParisPocGru

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        self.logger = logging.getLogger(__name__)
        url_prefix = '/notification/v0'
        self.ws = self.get_object()
        self.ws.get_access_token()
        self.ws.url += url_prefix
        self.json_data = json.loads(request.body)
        return super(DemandBaseView, self).dispatch(request, *args, **kwargs)

    def validate_data(self, fields):
        """Validate post data
        """
        payload = {}
        self.logger.debug("JSON DATA: %r" %self.json_data)
        missing_key = set(fields).difference(set(self.json_data.keys()))
        if missing_key:
            raise MissingKeyException('%s is required' %(missing_key.pop()))

        for field in fields:
            payload[field] = self.json_data[field]

        if payload.get('user_guid', None):
            payload['user_guid'] = str(uuid.UUID(payload['user_guid'], version=4))

        return payload

    def check_request_response(self, response):
        try:
            error = response.json().get('error', None)
            if error:
                raise DemandNotFoundException('demand not found')
        except (AttributeError,):
            return response


class CreateDemandView(DemandBaseView):

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(CreateDemandView, self).dispatch(request, *args, **kwargs)

    @utils.to_json('api')
    def post(self, request, *args, **kwargs):
        data = self.validate_data([
            'id_status_crm','id_demand_type',
            'status_text', 'user_guid'
        ])

        response = requests.post(self.ws.url + '/demand/create',
            headers = self.ws.headers,
            data = data
        )
        self.logger.debug("Dashboard response: %r - %r"%(response.status_code, response.content))
        demand_id = response.json()
        if demand_id < 0 : # -1, creation failed
            return {'err': 'demand creation failed'}

        display_id = response.json()
        return {'display_id': display_id}


class UpdateDemandView(DemandBaseView):

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(UpdateDemandView, self).dispatch(request, *args, **kwargs)

    @utils.to_json('api')
    def post(self, request, *args, **kwargs):
        data = self.validate_data([
            'id_demand', 'id_status_crm', 'status_text'
        ])

        response = requests.post(self.ws.url + '/demand/update',
            headers = self.ws.headers,
            data = data
        )

        self.logger.debug("Dashboard response: %r - %r"%(response.status_code, response.content))
        response = self.check_request_response(response)

        return {'display_id': response.json()}


class NotifyDemandView(DemandBaseView):

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(NotifyDemandView, self).dispatch(request, *args, **kwargs)

    @utils.to_json('api')
    def post(self, request, *args, **kwargs):
        data = self.validate_data([
            'id_demand', 'notification_object',
            'notification_message', 'notification_sender'
        ])

        response = requests.post(self.ws.url + '/demand/notify',
                headers = self.ws.headers,
                data = data
        )

        self.logger.debug("Dashboard response: %r - %r"%(response.status_code, response.content))
        response = self.check_request_response(response)

        return {'display_id': response.json()}


class DeleteDemandView(DemandBaseView):

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(DeleteDemandView, self).dispatch(request, *args, **kwargs)

    @utils.to_json('api')
    def post(self, request, *args, **kwargs):
        data = self.validate_data(['id_demand'])

        response = requests.post(self.ws.url + '/demand/delete',
                headers = self.ws.headers,
                data = data
        )

        self.logger.debug("Dashboard response: %r - %r"%(response.status_code, response.content))
        response = self.check_request_response(response)

        return {'display_id': response.json()}


# passerelle - uniform access to multiple data sources and services
# Copyright (C) 2015-2016  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import base64
import requests

from django.db import models
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _
from django.db import models

from passerelle.base.models import BaseResource


class ParisPocGru(BaseResource):
    url = models.CharField(max_length=128, blank=False,
        default="not provided", verbose_name=_('WebService Base Url')
    )
    consumer_key = models.CharField(max_length=128, blank=False,
        default="not provided", verbose_name=_('Consumer Key')
    )
    consumer_secret = models.CharField(max_length=128, blank=False,
        default="not provided", verbose_name=_('Consumer Secret')
    )
    authentic_username = models.CharField(_('Authentic username'), max_length=64)
    authentic_password = models.CharField(_('Authentic password'), max_length=64)
    wcs_user_email = models.EmailField(_('WCS user email'),
                            help_text=_('to authenticate against wcs'))
    category = _('Business Process Connectors')

    class Meta:
        verbose_name = _('Paris Poc GRU')

    def get_absolute_url(self):
        return reverse('paris-poc-gru-view', kwargs={'slug': self.slug})

    @classmethod
    def get_add_url(cls):
        return reverse('paris-poc-gru-add')

    @classmethod
    def get_verbose_name(cls):
        return cls._meta.verbose_name

    @classmethod
    def get_icon_class(cls):
        return 'ressources'

    def get_access_token(self):
        cred = base64.b64encode('%s:%s' %(self.consumer_key, self.consumer_secret))
        response = requests.post(
            self.url+'/token',
            headers = {
                'Authorization': 'Basic %s' %cred,
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data = {'grant_type': 'client_credentials'}
        )

        self.access_token = response.json()['access_token']
        self.headers = {'Authorization': 'Bearer %s' %(self.access_token)}

        return self.access_token

# passerelle - uniform access to multiple data sources and services
# Copyright (C) 2015-2016  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.conf.urls import patterns, include, url
from django.contrib.auth.decorators import login_required

from passerelle.urls_utils import decorated_includes, required, app_enabled

from views import *
from out_views import *


public_urlpatterns = patterns('',
    url(r'^(?P<slug>[\w,-]+)/$', ParisPocGruDetailView.as_view(), name='paris-poc-gru-view'),
    url(r'^(?P<slug>[\w,-]+)/endpoint/$', EndpointView.as_view(), name='paris-poc-gru-endpoint'),
    url(r'^(?P<slug>[\w,-]+)/create/$', CreateDemandView.as_view(), name='paris-poc-gru-create-demand'),
    url(r'^(?P<slug>[\w,-]+)/update/$', UpdateDemandView.as_view(), name='paris-poc-gru-update-demand'),
    url(r'^(?P<slug>[\w,-]+)/notify/$', NotifyDemandView.as_view(), name='paris-poc-gru-notify-demand'),
    url(r'^(?P<slug>[\w,-]+)/delete/$', DeleteDemandView.as_view(), name='paris-poc-gru-delete-demand'),
)

management_urlpatterns = patterns('',
    url(r'^add$', ParisPocGruCreateView.as_view(), name='paris-poc-gru-add'),
    url(r'^(?P<slug>[\w,-]+)/edit$', ParisPocGruUpdateView.as_view(), name='paris-poc-gru-edit'),
    url(r'^(?P<slug>[\w,-]+)/delete$', ParisPocGruDeleteView.as_view(), name='paris-poc-gru-delete'),
)

urlpatterns = required(
    app_enabled('passerelle_paris_poc_gru'),
    patterns('',
        url(r'^paris-poc-gru/', include(public_urlpatterns)),
        url(r'^manage/paris-poc-gru/',
            decorated_includes(login_required, include(management_urlpatterns))),
    )
)

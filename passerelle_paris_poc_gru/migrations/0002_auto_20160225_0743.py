# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('passerelle_paris_poc_gru', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='parispocgru',
            name='consumer_key',
            field=models.CharField(default=b'not provided', max_length=128, verbose_name='Consumer Key'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='parispocgru',
            name='consumer_secret',
            field=models.CharField(default=b'not provided', max_length=128, verbose_name='Consumer Secret'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='parispocgru',
            name='url',
            field=models.CharField(default=b'not provided', max_length=128, verbose_name='WebService Base Url'),
            preserve_default=True,
        ),
    ]

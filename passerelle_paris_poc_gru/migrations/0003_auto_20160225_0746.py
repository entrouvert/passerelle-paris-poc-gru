# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('passerelle_paris_poc_gru', '0002_auto_20160225_0743'),
    ]

    operations = [
        migrations.AddField(
            model_name='parispocgru',
            name='authentic_password',
            field=models.CharField(default='', max_length=64, verbose_name='Authentic password'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='parispocgru',
            name='authentic_username',
            field=models.CharField(default='', max_length=64, verbose_name='Authentic username'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='parispocgru',
            name='wcs_user_email',
            field=models.EmailField(default='', help_text='to authenticate against wcs', max_length=75, verbose_name='WCS user email'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='parispocgru',
            name='consumer_key',
            field=models.CharField(default=None, max_length=128, verbose_name='Consumer Key'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='parispocgru',
            name='consumer_secret',
            field=models.CharField(default=None, max_length=128, verbose_name='Consumer Secret'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='parispocgru',
            name='url',
            field=models.CharField(default=None, max_length=128, verbose_name='WebService Base Url'),
            preserve_default=True,
        ),
    ]
